\regularsection{Основные понятия и теоремы}

\subsection{Способы определения функции от матрицы}

Существуют различные определения функции от матрицы, но во многих случаях они эквивалентны.
Это упрощает получение теоретических результатов.
Существуют следующие определения:
\begin{itemize}
\item через ряд Тейлора,
\item через жорданову форму,
\item через эрмитов интерполяционный многочлен,
\item через обобщение интегральной теоремы Коши.
\end{itemize}

Проще всего определить $f(A)$ через подстановку $A$ в ряд Тейлора.
Пусть для некоторых $f: \mathbb{C} \to \mathbb{C}$ и $\alpha \in \mathbb{C}$
\begin{equation}
f(z) = \sum_{n=0}^\infty a_n(z - \alpha)^n,\ a_n = \frac{f^{(n)}(\alpha)}{n!}.
\label{taylor_expansion_at_alpha}
\end{equation}
Тогда для $A \in \cmat{n}$, где $\cmat{n}$~--- множество матриц над $\mathbb{C}$ размера $n \times n$, можно определить
\begin{equation}
f(A) \bydef \sum_{n=0}^\infty a_n(A - \alpha I)^n
\label{matrix_taylor_series}
\end{equation}
при условии, что для данной матрицы $A$ ряд \pref{matrix_taylor_series} сходится (имеется в виду абсолютная сходимость).
Такой способ является классическим способом определения функций $e^A$, $\cos A$ и $\sin A$, потому что соответствующие ряды сходятся для всех $A \in \cmat{n}$. В самом деле, несложно показать, что если ряд
\pref{taylor_expansion_at_alpha} имеет радиус сходимости $R$, то из $\norm{A - \alpha I} < R$ следует, что $\pref{matrix_taylor_series}$ сходится.

В качестве примера функции, которая плохо определяется через ряд Тейлора, можно привести ${\displaystyle\ln(I + A)}$.
Ряд
$$\sum_{n = 1}^\infty \frac{(-1)^{n+1}A^n}{n}$$
сходится при $\specrad{A} < 1$, где $\specrad{A}$~--- спектральный радиус $A$, а при $\specrad{A} > 1$ он расходится.

В общем случае сходимость \pref{matrix_taylor_series} зависит от радиуса сходимости \pref{taylor_expansion_at_alpha} и собственных чисел $A$; если матричный ряд сходится, то его сумма совпадает с $f(A)$ в смысле определения \pref{jordan_def} (см. \pref{convergence_of_matrix_taylor_series}).

Рассмотрим более общие определения $f(A)$, которые подходят для функций, которые не раскладываются в ряд Тейлора.

Пусть $A \in \cmat{n}$. Известно, что $A$ можно представить в жордановой форме:
$$Z^{-1}AZ = J = \diag(J_1, \dots, J_p),$$
где
\begin{equation}
J_k =
\begin{pmatrix}
\lambda_k & 1 & & \\
 & \lambda_k & \ddots &\\
 & & \ddots & 1\\
 & & & \lambda_k\\
\end{pmatrix}
\in \cmat{n_k}
\label{jordan_form}
\end{equation}
и $\displaystyle\sum_{k=1}^p n_k = n$.
Это хорошо описано, например, в \cite{vinberg} или \cite{axler}.

\begin{df}
Множество всех собственных чисел $A \in \cmat{n}$ называется \textit{спектром} $A$ и обозначается $\spec{A}$.
Пусть $A \in \cmat{n}$ имеет жорданову форму \pref{jordan_form} и пусть $\spec{A} = \{\lambda_1, \dots, \lambda_s\}$.
Пусть $n_k$ --- размер наибольшей жордановой клетки, у которой на диагонали $\lambda_k$.
$n_k$ называется \textit{индексом} $\lambda_k$.
Говорят, что \textit{$f$ определена на $\spec{A}$}, если для $k = \overline{1, s}$ существует производная $f^{(n_k - 1)}(\lambda_k)$.
Будем называть эти производные \textit{значениями $f$ на $\spec{A}$}.
\end{df}

\begin{df}\label{jordan_def}
Пусть $f$ определена на спектре $A \in \cmat{n}$ и имеет жорданову форму \pref{jordan_form}. Тогда
$$f(A) := Zf(J)Z^{-1} = Z\diag(f(J_1), \dots, f(J_p))Z^{-1},$$
где
\[
f(J_k) :=
\begin{pmatrix}
f(\lambda_k) & f'(\lambda_k) & \dots & \frac{f^{(n_k - 1)}(\lambda_k)}{(n_k - 1)!}\\
& f(\lambda_k) & \ddots & \vdots \\
&& \ddots & f'(\lambda_k) \\
&&& f(\lambda_k) \\
\end{pmatrix}.
\]
\end{df}

Заметим, что $f(A)$ однозначно определена, если $f$ однозначно определена на $\spec{A}$. Если $f$~--- многозначная функция (например, комплексный корень или логарифм), нужно для каждого $\lambda_k$ выбрать какую-то одну ветвь этой функции (назовем ее \textit{главной}). Более того, если $\lambda_k$ встречается больше чем в одной жордановой клетке, нужно в каждой жордановой клетке с $\lambda_k$ выбирать одну и ту же ветвь. Если функция получена таким способом, ее называют \textit{главной}. В противном случае ее называют \textit{побочной}.

Мотивация определения \pref{jordan_def} следующая.
Представим $J_k$ как
$$J_k = \lambda_k I + N_k.$$
При возведении $N_k$ в степень диагональ единиц будет смещаться в правый верхний угол и в конце концов $N_k^{n_k}$ будет равно 0.
Пусть $f$ раскладывается в ряд Тейлора в точке $\lambda_k$ как
$$f(t) = \sum_{j=0}^{\infty} \frac{f^{(j)}(\lambda_k)(t - \lambda_k)^j}{j!}.$$
Подставляя $J_k$ вместо $t$, получаем
$$f(J_k) = \sum_{j=0}^{n_k - 1} \frac{f^{(j)}(\lambda_k)N_k^j}{j!}.$$
Можно показать, что классическое определение согласуется с определением через жорданову форму:

\begin{prop}\label{convergence_of_matrix_taylor_series}
Пусть $f$ раскладывается в ряд Тейлора
$$f(z) = \sum_{j=0}^\infty a_j(z - \alpha)^j,\ a_j = \frac{f^{(j)}(\alpha)}{j!}$$
с радиусом сходимости $R$ и $A \in \cmat{n}$. Тогда $f(A)$ определена на $\spec{A}$ и
$$f(A) = \sum_{j=0}^\infty a_j(A - \alpha I)^j$$
в том и только том случае, если для всех $\lambda_i \in \spec{A}$ выполняется одно из условий
\begin{enumerate}
\item $|\lambda_i - \alpha| < R$;
\item $|\lambda_i - \alpha| = R$ и ряд Тейлора для $f^{(n_i - 1)}(\lambda)$, где $n_i$~--- индекс $\lambda_i$, сходится в точке $\lambda = \lambda_i$, $i = \overline{1, s}$.
\end{enumerate}
\end{prop}
\begin{proof}
См. \cite{higham}.
\end{proof}

Многие теоретические результаты легко следуют из того факта, что любая функция от матрицы~--- это многочлен. Сначала покажем, что значения $f$ на $\spec{A}$ однозначно определяют многочлен, которым мы хотим интерполировать~$f$.
\begin{lmm}\label{root_multiplicity_lemma}
Пусть $p$~--- многочлен над $\mathbb{C}$ степени $n$ и $c_k$~--- корень $p$.
Тогда если $m < n$, то
$$p(c_k) = p'(c_k) = \dots = p^{(m)}(c_k) = 0$$
в том и только том случае, если $\mu(c_k) \geq m + 1$, где $\mu(c_k)$~--- кратность $c_k$.
\end{lmm}
\begin{proof}
Достаточно доказать, что $\mu(c_k)$ уменьшается на 1 при дифференцировании $p$.
Пусть $c_1, \dots, c_n$~--- различные корни $p$.
Обозначим $\mu = \mu(c_k)$ и запишем 
$$p(z) = (z - c_k)^\mu q(z),$$
где $q(z)$~--- это многочлен такой, что $q(c_k) \neq 0$.
Тогда
\begin{equation*}
\begin{split}
p'(z) & = \mu(z - c_k)^{\mu - 1}q(z) + (z - c_k)^\mu q'(z) \\
& = (z - c_k)^{\mu - 1}[\mu q(z) + (z - c_k)q'(z)].
\end{split}
\end{equation*}
Теперь кратность $c_k$ равна $\mu - 1$, потому что
$$[\mu q(z) + (z - c_k)q'(z)](c_k) = \mu q(c_k) \neq 0.$$
\end{proof}

\begin{prop}\label{pA_equals_qA_iff_p_equals_q_on_specA}
Пусть $A \in \cmat{n}$, $p$ и $q$ --- многочлены.
Тогда $p(A) = q(A)$ в том и только том случае, если $p$ и $q$ принимают одинаковые значения на $\spec{A}$.
\end{prop}
\begin{proof}
Положим $d = p - q$ и обозначим минимальный многочлен $A$ как $\psi$.
Так как $\psi$ делит любой аннулирующий многочлен $A$,
достаточно показать, что $\psi \divides d$ тогда и только тогда, когда $d$ обращается в 0 на $\spec{A}$, то есть
$$d(\lambda_i) = d'(\lambda_i)= \dots = d^{(n_i - 1)}(\lambda_i) = 0,\ i = \overline{1, s},\eqno(*)$$
где $\lambda_1, \dots, \lambda_s$ --- различные собственные числа $A$, а $n_i$ --- индекс $\lambda_i$.
Из жордановой формы \pref{jordan_form} ясно, что
$$\psi(z) = \prod_{i=1}^s(z - \lambda_i)^{n_i}.$$
Поэтому $\psi \divides d$ тогда и только тогда, когда для $d$ выполняется
$$\mu(\lambda_i) \geq n_i, i = \overline{1, s}.$$
А по \pref{root_multiplicity_lemma} это равносильно (*).
\end{proof}

\begin{df}\label{polynomial_def}
Пусть $A \in \cmat{n}$, $f$ определена на $\spec{A}$ и $n_1, \dots, n_s$ --- индексы собственных чисел $A$.
Тогда $f(A) := p(A)$, где $p$ --- многочлен степени меньшей чем $\sum n_k$, удовлетворяющий условиям
$$p^{(j)}(\lambda_k) = f^{(j)}(\lambda_k),\ j = \overline{0, n_k - 1},\ k = \overline{1, s}.$$
Такой многочлен $p$ единственный и он называется эрмитовым интерполяционным многочленом.
\end{df}

Существует еще одно определение.
\begin{df}\label{integral_def}
Пусть $A \in \cmat{n}$, $\Gamma$ --- замкнутый контур, содержащий $\spec{A}$, $f$ аналитична на $\Gamma$ и внутри $\Gamma$.
Тогда
$$f(A) := \frac{1}{2\pi i}\int_\Gamma f(z)(zI - A)^{-1}dz.$$
\end{df}

\begin{prop}
Определения \pref{jordan_def} и \pref{polynomial_def} эквивалентны.
Если $f$ аналитична, то определение \pref{integral_def} эквивалентно \pref{jordan_def} и \pref{polynomial_def}.
\end{prop}
\begin{proof}
См. \cite{higham}.
\end{proof}

Докажем некоторые свойства $f(A)$, которые понадобятся нам в дальнейшем.
\begin{prop}\label{fA_properties}
Пусть $A \in \cmat{n}$, $f$ определена на $\spec{A}$. Тогда
\begin{itemize}
\item[(а)] $Af(A) = f(A)A$;
\item[(б)] если $XA = AX$, то $Xf(A) = f(A)X$;
\item[(в)] если $A = (A_{ij})$ блочно-треугольная, то $F = f(A)$ блочно-треугольная с такой же структурой блоков, как у $A$, и $F_{ii} = f(A_{ii})$.
\end{itemize}
\end{prop}
\begin{proof} Так как $f(A) = p(A)$ для некоторого многочлена $p$, имеем
$$Af(A) = Ap(A) = p(A)A = f(A)A.$$
(б) доказывается так же, потому что если $XA = AX$, то $Xp(A) = p(A)X$.
Чтобы доказать (в), заметим, что структура блоков сохраняется при сложении и умножении матриц, а значит, $F = f(A) = p(A)$ имеет такую же структуру как $A$ и $F_{ii} = p(A_{ii})$. Из \pref{pA_equals_qA_iff_p_equals_q_on_specA} следует, что $p(A_{ii}) = f(A_{ii})$.
\end{proof}

\subsection{Числа обусловленности матричных функций}

Для анализа устойчивости методов вычисления $f(A)$ используются \textit{числа обусловленности}. Они показывают как сильно маленькие изменения аргумента могут изменить значение функции.
Стандартное определение относительного числа обусловленности функции $f: \mathbb{R} \to \mathbb{R}$:
$$\condrel(f, x) \bydef \lim_{\varepsilon \to 0} \sup_{|\Delta x| \leq \varepsilon |x|} \left|\frac{f(x + \Delta x) - f(x)}{\varepsilon f(x)}\right|.$$

Рассмотрим матричную функцию $f: \cmat{n} \to \cmat{n}$. \textit{Относительное число обусловленности} $f$ определяется как
$$\condrel(f, X) \bydef \lim_{\varepsilon \to 0} \sup_{\norm{E} \leq \varepsilon\norm{X}} \frac{\norm{f(X + E) - f(X)}}{\varepsilon\norm{f(X)}}.$$
Из определения следует, что
\begin{equation}\label{condrel_bound}
\frac{\norm{f(X + E) - f(X)}}{\norm{f(X)}} \leq \condrel(f, X)\frac{\norm{E}}{\norm{X}} + o(\norm{E}).
\end{equation}
\textit{Абсолютное число обусловленности} $f$ определяется как
$$\condabs(f, X) \bydef \lim_{\varepsilon \to 0} \sup_{\norm{E} < \varepsilon} \frac{\norm{f(X + E) - f(X)}}{\varepsilon}.$$
Оно отличается от относительного на константный множитель:
$$\condrel(f, X) = \condabs(f, X)\frac{\norm{X}}{\norm{f(X)}}.$$
Сопоставляя это с \pref{condrel_bound}, получаем
\begin{equation}\label{condabs_bound}
\norm{f(X + E) - f(X)} \leq \condabs(f, X)\norm{E} + o(\norm{E}).
\end{equation}

$\condrel(f, X)$ и $\condabs(f, X)$ можно выразить через производную Фреше.
Производной Фреше функции $f: \cmat{n} \to \cmat{n}$ в точке $X \in \cmat{n}$ называется линейное отображение $L_X: \cmat{n} \to \cmat{n}$ такое, что
$$f(X + E) - f(X) - L_X(E) = o(\norm{E})$$
для всех $E \in \cmat{n}$.
\begin{prop}
Определим норму $L(X)$ как
$$\norm{L(X)} \bydef \sup_{Z \neq 0} \frac{\norm{L_X(Z)}}{\norm{Z}}.$$
Тогда
\begin{equation}\label{condabs_frechet}
\condabs(f, X) = \norm{L(X)}
\end{equation}
и
\begin{equation}\label{condrel_frechet}
\condabs(f, X) = \frac{\norm{L(X)}\norm{X}}{\norm{f(X)}}.
\end{equation}
\end{prop}
\begin{proof}
См. \cite{higham}.
\end{proof}

\subsection{Сигнум-функция}

Впервые сигнум-функция была использована Робертсом \cite{roberts} для решения уравнений Ляпунова и алгебраических уравнений Риккати, которые возникают в теории управления.
Для скалярных $z$ сигнум-функция определяется как
\[
\sign(z) =
\begin{cases}
1, & \re z > 0\\
-1, & \re z < 0.
\end{cases}
\]
Пусть $A \in \cmat{n}$ имеет жорданову форму $A = ZJZ^{-1}$. Расположим жордановы клетки таким образом, что
\[J =
\begin{pmatrix}
J_1 & 0\\
0 & J_2
\end{pmatrix},
\]
где собственные числа $J_1 \in \cmat{p}$ и $J_2 \in \cmat{q}$ лежат в левой и правой открытых полуплоскостях соответственно. Тогда по определению \pref{jordan_def}
\[
\sign(A) = Z
\begin{pmatrix}
-I_p & 0\\
0 & I_q
\end{pmatrix}
Z^{-1},
\]
где $I_n$ означает единичную матрицу размера $n \times n$.

Применения сигнум-функции и методы ее вычисления будут рассмотрены в следующих главах.

\subsection{Выводы по разделу №1}

Естественным обобщением определения $f(A)$ для диагонализуемых матриц на недиагонализуемые является определение \pref{jordan_def} через жорданову форму. Определение выбрано так, что во многих случаях оно согласуется с классическим определением $f(A)$ через ряд Тейлора. Для получения теоретических результатов также удобно использовать тот факт, что любая функция от матрицы~--- это многочлен.

Для анализа устойчивости методов вычисления функций от матриц используются числа обсуловленности, которые определяются по аналогии с числами обсуловленности функций из $\mathbb{R}$ в $\mathbb{R}$.

С помощью определения через жорданову форму мы дали первое определение $\sign(A)$.
