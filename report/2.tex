\regularsection{Применения сигнум-функции}

\subsection{Алгебраические уравнения Риккати и их решение с помощью сигнум-функции}

Рассмотрим алгебраическое уравнение Риккати
\begin{equation}
	AX + XB - XCX + Q = 0,
	\label{riccati_equation}
\end{equation}
в котором все матрицы имеют размер $n \times n$.
Это уравнение можно записать как
\begin{equation}
	\begin{pmatrix}
	I & -X
	\end{pmatrix}
	H
	\begin{pmatrix}
	X\\
	I
	\end{pmatrix}
	= 0,
	\label{riccati_equation_with_characteristic_matrix}
\end{equation}
где
\[H = \begin{pmatrix}
A & Q\\
C & -B
\end{pmatrix}.\]
Имеет место следующий интересный факт:
\begin{lmm}\label{matrix_ring_lemma}
Если $X$~--- решение \pref{riccati_equation_with_characteristic_matrix} и $f$ определена на $\spec{H}$, то
\[\begin{pmatrix}
I & -X
\end{pmatrix}
f(H)
\begin{pmatrix}
X\\
I
\end{pmatrix}
= 0.\]
\end{lmm}
\begin{proof}
См. \cite{evnin}.
\end{proof}

Для простоты рассмотрим уравнение Сильвестра, которое является частным случаем (\ref{riccati_equation}):
$$AX + XB + Q = 0.$$
Его можно переписать как
\begin{equation}
\begin{pmatrix}
I & -X
\end{pmatrix}
H
\begin{pmatrix}
X\\
I
\end{pmatrix}
= 0,
\label{silvester_equation_with_characteristic_matrix}
\end{equation}
где
\[H = 
\begin{pmatrix}
A & Q\\
0 & -B
\end{pmatrix}.\]
По лемме \pref{matrix_ring_lemma}
\[\begin{pmatrix}
I & -X
\end{pmatrix}
\sign(H)
\begin{pmatrix}
X\\
I
\end{pmatrix}
= 0,\]
где
\[\sign(H) = 
\begin{pmatrix}
\sign(A) & G\\
0 & -\sign(B)
\end{pmatrix}.\]
для некоторой $G \in \cmat{n}$. Отсюда
$$\sign(A)X + X\sign(B) + G = 0.$$
Если $A$ и $B$ положительно определенные матрицы, то $\sign(A) = \sign(B) = I$ и $X = -0.5G$.

Аналогичным образом уравнения Риккати в общем случае сводятся к вычислению сигнум-функции от матрицы $2n \times 2n$ и решению переопределенной, но совместной системы линейных уравнений.

\subsection{Связь с другими функциями}

Рассмотрим функцию $A^{1/2}$~--- квадратный корень из $A$. Квадратным корнем из $A$ будем называть любое решение уравнения $X^2 = A$. В общем случае $A$ может иметь бесконечно много квадратных корней. Например, любая матрица поворота на $\pi$ вокруг какой-нибудь оси в трехмерном пространстве является квадратным корнем из $I_3$.
Главным квадратным корнем называется функция, полученная в соответствии с комментарием к определению \pref{jordan_def}. Ясно, что $\sign(A)$~--- это квадратный корень из $I$ (в общем случае побочный), который зависит от собственных чисел $A$.

Заметим, что $\sign(A)$ можно представить в виде
\begin{equation}\label{sign_through_sqrt}
\sign(A) = A(A^2)^{-1/2}.
\end{equation}
Эта формула обобщает формулу для скалярных $z$
$$\sign(z) = \frac{z}{\sqrt{z^2}}.$$
Следующая теорема позволяет вычислить квадратный корень из матрицы через сигнум-функцию.
\begin{prop}\label{nonprimary_diagonal_sign}
	Пусть $A, B \in \cmat{n}$ и пусть $AB$, $BA$ не имеют вещественных отрицательных собственных чисел. Тогда
	\[
		\sign
		\begin{pmatrix}
		0 & A\\
		B & 0
		\end{pmatrix} = 
		\begin{pmatrix}
		0 & C\\
		C^{-1} & 0
		\end{pmatrix},
	\]
	где $C = A(BA)^{-1/2}$.
\end{prop}
\begin{proof}
	Обозначим
	\[
		P =
		\begin{pmatrix}
			0 & A \\
			B & 0
		\end{pmatrix}.
	\]
	$P$ не имеет собственных чисел на мнимой оси, потому что если бы это было так,
	\[
		P^2 = 
		\begin{pmatrix}
			AB & 0 \\
			0 & BA
		\end{pmatrix}
	\]
	имела бы отрицательные вещественные собственные числа.
	\begin{equation*}
	\begin{split}
		\sign(P) & = P(P^2)^{-1/2}\\
		& = 
		\begin{pmatrix}
			0 & A \\
			B & 0
		\end{pmatrix}
		\begin{pmatrix}
			AB & 0 \\
			0 & BA
		\end{pmatrix}^{-1/2}\\
		& =
		\begin{pmatrix}
			0 & A \\
			B & 0
		\end{pmatrix}
		\begin{pmatrix}
			(AB)^{-1/2} & 0 \\
			0 & (BA)^{-1/2}
		\end{pmatrix}\\
		& = 
		\begin{pmatrix}
			0 & C \\
			D & 0
		\end{pmatrix},
	\end{split}
	\end{equation*}
	где $D = B(AB)^{-1/2}$.
	Заметим, что
	\[
		\begin{pmatrix}
			0 & C \\
			D & 0
		\end{pmatrix}^2 = I,
	\]
	откуда $CD = DC = I$, то есть $D = C^{-1}$.
\end{proof}
Вычисление $A^{-1/2}$ можно свести к вычислению сигнум-функции следующим образом:
\[
\sign
\begin{pmatrix}
0 & A\\
I & 0
\end{pmatrix} = 
\begin{pmatrix}
0 & A^{1/2}\\
A^{-1/2} & 0
\end{pmatrix}.
\]

Таким же образом можно вычислить \textit{полярное разложение} матрицы.
Пусть $A \in \cmat{n}$. Известно \cite{axler}, что $A$ можно представить в виде $A = UH,$ где $U$~--- унитарная матрица, $H = (A^*A)^{-1/2}$~--- эрмитова матрица.
По теореме \pref{nonprimary_diagonal_sign}
\[
	\sign
	\begin{pmatrix}
		0 & A\\
		A^* & 0
	\end{pmatrix}
	=
	\begin{pmatrix}
		0 & U\\
		U^* & 0
	\end{pmatrix},
\]
так как $A(A^*A)^{-1/2} = AH^{-1} = U$. Далее $H = U^{-1}A = U^TA$.

Вычисление корня степени $p$ из матрицы также можно свести к вычислению сигнум-функции \cite{higham}.

\subsection{Выводы по разделу №2}

$\sign(A)$ может использоваться для решения матричных уравнений.
Связь $\sign(A)$ с корнем из матрицы и полярным разложением обсуловлена соотношением $\sign(A) = A(A^2)^{-1/2}$.
Теорема \pref{nonprimary_diagonal_sign} позволяет нам вычислять корень из $A$ и полярное разложение $A$ через $\sign(A)$.
