function [S, iteration_count] = pade_k2_m2(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    
    n = size(A, 1);
    I = eye(n);
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        X_next = X * (5 * I + 10 * X^2 + X^4) / (I + 10 * X^2 + 5 * X^4);
        err = norm(X_next - X);
        X = X_next;
    end
    
    S = X;
end
