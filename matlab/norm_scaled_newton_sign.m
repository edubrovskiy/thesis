function [S, iteration_count] = norm_scaled_newton_sign(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        X_inv = inv(X);
        sc = sqrt(norm(X_inv) / norm(X));
        
        X_next = 0.5 * (sc * X + X_inv / sc);
        err = norm(X_next - X);
        X = X_next;  
    end
    
    S = X;
end
