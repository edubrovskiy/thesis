function [S, iteration_count] = scaled_pade(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    
    n = size(A, 1);
    I = eye(n);
    
    while err > maxerr
        iteration_count = iteration_count + 1;
        
        [~, U] = lu(X);
        sc = exp(-1/n * sum(log(abs(diag(U)))));

        %sc
        X_next = sc * X * (5 * I + 10 * sc^2 * X^2 + sc^4 * X^4) / (I + 10 * sc^2 * X^2 + 5 * sc^4 * X^4);
        err = norm(X_next - X);
        X = X_next;
    end
    
    S = X;
end
