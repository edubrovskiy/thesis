function [S, iteration_count] = spec_scaled_newton_sign(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    n = size(A, 1);
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        l = abs(eig(X));
        sc = 1 / sqrt(max(l) * min(l));
        
        X_next = 0.5 * (sc * X + inv(X) / sc);
        err = norm(X_next - X);
        X = X_next;  
    end
    
    S = X;
end
