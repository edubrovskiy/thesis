function [S, iteration_count] = det_scaled_newton_sign(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    n = size(A, 1);
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        [~, U] = lu(X);
        sc = exp(-1/n * sum(log(abs(diag(U)))));
        
        X_next = 0.5 * (sc * X + inv(X) / sc);
        err = norm(X_next - X);
        X = X_next;  
    end
    
    S = X;
end
