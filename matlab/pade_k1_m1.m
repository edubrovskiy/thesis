function [S, iteration_count] = pade_k1_m1(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    
    n = size(A, 1);
    I = eye(n);
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        X_next = X * (3 * I + X^2) / (I + 3 * X^2);
        err = norm(X_next - X);
        X = X_next;
    end
    
    S = X;
end
