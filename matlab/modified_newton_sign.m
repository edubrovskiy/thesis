function [S, iteration_count] = modified_newton_sign(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    I = eye(size(A, 1));
    while err > maxerr
        iteration_count = iteration_count + 1;

        X_next = 0.5 * X * (3 * I - X^2);
        err = norm(X_next - X);
        X = X_next;  
    end
    
    S = X;
end
