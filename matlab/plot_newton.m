grid;
hold on;

n = 10;
newton_it_count = zeros(n);
norm_scaled_newton_it_count = zeros(n);
det_scaled_newton_it_count = zeros(n);
spec_scaled_newton_it_count = zeros(n);
pade_k1_m1_it_count = zeros(n);
pade_k2_m2_it_count = zeros(n);
scaled_pade_it_count = zeros(n);

maxerr = 1e-6;

for i = 1:n
    sz = i * 10;
    A = 1e6 * (2 * rand(sz) - 1);
    [~, newton_it_count(i)] = newton_sign(A, maxerr);
    [~, norm_scaled_newton_it_count(i)] = norm_scaled_newton_sign(A, maxerr);
    [~, det_scaled_newton_it_count(i)] = det_scaled_newton_sign(A, maxerr);
    [~, spec_scaled_newton_it_count(i)] = spec_scaled_newton_sign(A, maxerr);
    [~, pade_k1_m1_it_count(i)] = pade_k1_m1(A, maxerr);
    [~, pade_k2_m2_it_count(i)] = pade_k2_m2(A, maxerr);
    [~, scaled_pade_it_count(i)] = scaled_pade(A, maxerr);
end

plot(1:n, newton_it_count);
text(5, newton_it_count(5), 'unscaled');

plot(1:n, norm_scaled_newton_it_count);
plot(1:n, det_scaled_newton_it_count);
plot(1:n, spec_scaled_newton_it_count);

plot(1:n, pade_k1_m1_it_count);
text(5, pade_k1_m1_it_count(5), 'padek1m1');

plot(1:n, pade_k2_m2_it_count);
text(5, pade_k2_m2_it_count(5), 'padek2m2');

plot(1:n, scaled_pade_it_count);
text(5, scaled_pade_it_count(5), 'scaled_pade');
