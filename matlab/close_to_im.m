function A = close_to_im(n, re)
    A = diag(repmat(1i + re, 1, n));
end