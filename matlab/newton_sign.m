function [S, iteration_count] = newton_sign(A, maxerr)
    iteration_count = 0;
    err = Inf;
    X = A;
    
    n = size(A);
    I = eye(n);
    while err > maxerr && norm(I - X^2) >= 1
        iteration_count = iteration_count + 1;

        X_next = 0.5 * (X + inv(X));
        err = norm(X_next - X);
        X = X_next;  
    end
    
    while err > maxerr
        iteration_count = iteration_count + 1;

        X_next = 0.5 * X * (3 * I - X^2);
        err = norm(X_next - X);
        X = X_next;  
    end
    
    S = X;
end
