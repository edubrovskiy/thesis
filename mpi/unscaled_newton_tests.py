#!/usr/bin/python3
import sys, os, filecmp

def make_matrix(size, path):
    os.system("python3 matrix_utils/make_matrix.py {0} > {1}.txt && matrix_utils/mtob {1}.txt {1}.in".format(size, path))

def run_test(n, path, proc_count, test):
    cmd = "mpirun -n {0} bin/unscaled_newton {1} {2}.in res.out".format(proc_count, n, path)
    print(cmd)
    os.system(cmd)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: {0} <matrix_size> <test_count> <proc_count>".format(sys.argv[0]))
        sys.exit(1)

    (n, test_count, proc_count) = [int(arg) for arg in sys.argv[1:len(sys.argv)]]
    for i in range(test_count):
        path = "tests/{0}".format(i)
        make_matrix(n, path)

        run_test(n, path, proc_count, i)
