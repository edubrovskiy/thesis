#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

template<typename T>
void read_matrix(int n, const char *path) {
	FILE *f = fopen(path, "rb");

	T *buf = new T[n * n];
	fread(buf, sizeof(T), n * n, f);
	fclose(f);
	
	T **A = new T*[n];
	for (int i = 0; i < n; i++) {
		A[i] = new T[n];
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			A[i][j] = buf[i * n + j];
			std::cout << A[i][j] << '\t';
		}
		std::cout << std::endl;		
	}

	delete[] buf;
}

int main(int argc, char *argv[]) {
	if (argc < 3) {
		printf("usage: %s <matrix_size> <input_file>\n", argv[0]);
		return 1;
	}

	int n = atoi(argv[1]);
	read_matrix<double>(n, argv[2]);

	return 0;
}