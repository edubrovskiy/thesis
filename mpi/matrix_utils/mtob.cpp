#include <iostream>
#include <fstream>

int main(int argc, char *argv[]) {
	const char *input_path = argv[1];
	const char *output_path = argv[2];

	std::ifstream in(input_path);
	int n;
	in >> n >> n;
	double *buf = new double[n * n];

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			in >> buf[i * n + j];
		}
	}

	FILE *f = fopen(output_path, "wb");
	fwrite(buf, sizeof(double), n*n, f);
	fclose(f);

	delete buf;

	return 0;
}