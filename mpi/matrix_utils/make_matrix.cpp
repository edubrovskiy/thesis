#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[]) {
	if (argc < 3) {
		printf("usage: %s <matrix_size> <output_file>\n", argv[0]);
		return 1;
	}

	int n = std::atoi(argv[1]);

	double *buf = new double[n * n];
	srand(time(NULL));
	for (int i = 0; i < n * n; i++) {
		buf[i] = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
	}

	const char *path = argv[2];
	FILE *file = fopen(path, "w");
	if (file == NULL) {
		printf("error: failed to open %s for writing\n", path);
		return 1;
	}

	fwrite(buf, sizeof(double), n * n, file);
	delete[] buf;
	return 0;
}