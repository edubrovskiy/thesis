#!/usr/bin/python3
import sys, os, random

if __name__ == "__main__":
	if len(sys.argv) < 2:
		raise ValueError('Usage: {0} <matrix_size>'.format(sys.argv[0]))

	size = int(sys.argv[1])
	print("{0} {1}".format(size, size))
	for _ in range(size):
		for _ in range(size):
			sys.stdout.write("{0:.6f}\t".format(random.uniform(-1, 1)))
		print()
