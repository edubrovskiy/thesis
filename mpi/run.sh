#!/bin/bash

#SBATCH -p quick
#SBATCH -N 32
#SBATCH -t 120

mpirun bin/matrix_mult $1 $2 $3 $4
