#include <iostream>
#include <string>
#include <memory>
#include <vector>

namespace SignFunction {
	class Matrix {
	public:
		Matrix(int szi, int szj);
		Matrix(const Matrix&) = delete;
		Matrix& operator=(const Matrix&) = delete;
		Matrix(Matrix &&);

		Matrix clone() const;

		static std::shared_ptr<Matrix> identity(int n);

		void* get_buf();

		int get_szi() const;

		int get_szj() const;

		void* get_row_start(int i);

		bool is_square() const;

		bool compatible_with(const Matrix &other) const;

		Matrix multiply(const Matrix &other) const;

		Matrix add(const Matrix &other) const;

		Matrix subtract(const Matrix &other) const;

		std::string to_string() const;

		bool equals(const Matrix &other, double eps) const;

		double get(int i, int j) const;

		void set(int i, int j, double val);

		bool lu(std::vector<std::pair<int, int>> &swaps);

		double norm1() const;

		void swap(int i, int j);

		void swap(const std::vector<std::pair<int, int>> &swaps);

		Matrix submatrix(int i_start, int j_start, int row_count, int col_count) const;

		void submatrix(int i_start, int j_start, Matrix &B) const;

		void update_submatrix(int i_start, int j_start, const Matrix &B);

		~Matrix();
	private:
		double **a;
		int szi, szj;
		bool out_of_range(int i, int j) const;
	};
}
