#include "proc_grid_info.h"
#include <math.h>
#include <algorithm>

namespace SignFunction {
	int floor_sqrt(int x) {
		int r = (int)sqrt(x);
		while (r * r <= x) {
			r++;
		}

		return r - 1;
	}

	ProcGridInfo::ProcGridInfo(int n, int world_size, int rank) {
		this->n = n;
		this->world_size = world_size;
		this->rank = rank;

		proc_grid_size = std::min(floor_sqrt(world_size), n);
		proc_i = rank / proc_grid_size;
		proc_j = rank % proc_grid_size;

		MPI_Comm_split(MPI_COMM_WORLD, proc_i, rank, &row_comm);
		MPI_Comm_split(MPI_COMM_WORLD, proc_j, rank, &col_comm);

		block_size = n / proc_grid_size;

		i_start = proc_i * block_size + std::min(proc_i, n % proc_grid_size);
		row_count = proc_i < n % proc_grid_size
			? block_size + 1
			: block_size;

		j_start = proc_j * block_size + std::min(proc_j, n % proc_grid_size);
		col_count = proc_j < n % proc_grid_size
			? block_size + 1
			: block_size;

		lu_j_start = rank * (n / world_size) + std::min(rank, n % world_size);
		lu_col_count = rank < n % world_size
			? n / world_size + 1
			: n / world_size;
	}
}
