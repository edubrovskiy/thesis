#include "proc_grid_info.h"

namespace SignFunction {
	void read_submatrix(const std::string &path, int64_t displ, int n, int count, Matrix &A) {
		MPI_File file;
		MPI_File_open(MPI_COMM_WORLD, (char*)path.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &file);

		MPI_Datatype type;
		MPI_Type_vector(A.get_szi(), A.get_szj(), n, MPI_DOUBLE, &type);
		MPI_Type_commit(&type);

		MPI_File_set_view(file, displ, MPI_DOUBLE, type, (char*)"native", MPI_INFO_NULL);

		MPI_File_read_all(file, A.get_buf(), count, MPI_DOUBLE,	MPI_STATUS_IGNORE);

		MPI_File_close(&file);
		MPI_Type_free(&type);
	}

	void read_submatrix(const std::string &path, int64_t displ, int n, Matrix &A) {
		read_submatrix(path, displ, n, A.get_szi() * A.get_szj(), A);
	}

	Matrix read_horizontal_strip(const std::string &path, const ProcGridInfo &pginfo) {	
		Matrix A(pginfo.row_count, pginfo.n);

		int64_t displ = (int64_t)pginfo.i_start * pginfo.n * sizeof(double);
		int count = pginfo.proc_j == 0 ? A.get_szi() * A.get_szj() : 0;
		read_submatrix(path, displ, pginfo.n, count, A);

		return A;
	}

	Matrix read_vertical_strip(const std::string &path, const ProcGridInfo &pginfo) {
		Matrix A(pginfo.n, pginfo.col_count);

		int displ = pginfo.j_start * sizeof(double);
		int count = pginfo.proc_i == 0 ? A.get_szi() * A.get_szj() : 0;
		read_submatrix(path, displ, pginfo.n, count, A);

		return A;
	}

	void write_submatrix_at(Matrix &A, const std::string &path, int64_t displ, int n) {
		MPI_File file;
		MPI_File_open(MPI_COMM_WORLD, (char*)path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &file);

		MPI_Datatype type;
		MPI_Type_vector(A.get_szi(), A.get_szj(), n, MPI_DOUBLE, &type);
		MPI_Type_commit(&type);

		MPI_File_set_view(file,	displ, MPI_DOUBLE, type, (char*)"native", MPI_INFO_NULL);
		MPI_File_write_all(file, A.get_buf(), A.get_szi() * A.get_szj(), MPI_DOUBLE, MPI_STATUS_IGNORE);

		MPI_File_close(&file);
		MPI_Type_free(&type);
	}

	// void write_submatrix_at(Matrix &A, const std::string &path, const ProcGridInfo &pginfo) {
	// 	MPI_File file;
	// 	MPI_File_open(MPI_COMM_WORLD, (char*)path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &file);

	// 	MPI_Datatype type;
	// 	MPI_Type_vector(pginfo.row_count, pginfo.col_count, pginfo.n, MPI_DOUBLE, &type);
	// 	MPI_Type_commit(&type);

	// 	int64_t displ = ((int64_t)pginfo.i_start * pginfo.n + pginfo.j_start) * sizeof(double);
	// 	MPI_File_set_view(file,	displ, MPI_DOUBLE, type, (char*)"native", MPI_INFO_NULL);

	// 	int count = pginfo.row_count * pginfo.col_count;
	// 	MPI_File_write_all(file, A.get_buf(), count, MPI_DOUBLE, MPI_STATUS_IGNORE);

	// 	MPI_File_close(&file);
	// 	MPI_Type_free(&type);
	// }

#ifdef SEQUENTIAL

	std::shared_ptr<Matrix> sequential_read_matrix(int n, const char *path) {
		std::shared_ptr<Matrix> A(new Matrix(n, n));
		
		FILE *f = fopen(path, "rb");
		fread(A->get_buf(), sizeof(double), n * n, f);
		fclose(f);
		
		return A;
	}

	void sequential_write_matrix(const char *path, Matrix &A) {
		FILE *f = fopen(path, "wb+");
		fwrite(A.get_buf(), sizeof(double), A.get_szi() * A.get_szj(), f);
		fclose(f);
	}

#endif
}
