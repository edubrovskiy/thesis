#ifndef PROC_INFO_CPP
#define PROC_INFO_CPP

#include <mpi.h>

namespace SignFunction {
	class ProcInfo {
	public:
		ProcInfo(int n, int world_size, int rank) : world_size(world_size), rank(rank), n(n) {
			worker_count = std::min(world_size, n);

			j_start = rank * (n / worker_count) + std::min(rank, n % worker_count);
			
			col_count = rank < n % worker_count
				? n / worker_count + 1
				: n / worker_count;

			MPI_Comm_split(MPI_COMM_WORLD, rank < worker_count, rank, &worker_comm);
		}

		int world_size,
			worker_count,
			rank,
			n,						// matrix size
			col_count,
			j_start;

		MPI_Comm worker_comm;
	};
}

#endif