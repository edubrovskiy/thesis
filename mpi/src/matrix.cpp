#include "matrix.h"
#include <stdexcept>
#include "utils/alloc_contiguous.h"
#include <sstream>
#include <iomanip>
#include <cassert>
#include <cmath>

namespace SignFunction {
	Matrix::Matrix(int szi, int szj) : szi(szi), szj(szj) {
		a = Utils::alloc_contiguous<double>(szi, szj);
	}

	Matrix::Matrix(Matrix &&other) {
		szi = other.szi;
		szj = other.szj;

		a = other.a;
		other.a = nullptr;
	}

	Matrix Matrix::clone() const {
		Matrix B(szi, szj);
		for (int i = 0; i < szi; i++) {
			for (int j = 0; j < szj; j++) {
				B.a[i][j] = a[i][j];
			}
		}
		return B;
	}

	std::shared_ptr<Matrix> Matrix::identity(int n) {
		if (n <= 0) {
			throw std::invalid_argument("n <= 0");
		}

		std::shared_ptr<Matrix> I(new Matrix(n, n));
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				I->a[i][j] = 0;
			}
		}

		for (int i = 0; i < n; i++) {
			I->a[i][i] = 1;
		}

		return I;
	}

	void* Matrix::get_buf() {
		return a[0];
	}

	int Matrix::get_szi() const {
		return szi;
	}

	int Matrix::get_szj() const {
		return szj;
	}

	bool Matrix::is_square() const {
		return szi == szj;
	}

	bool Matrix::compatible_with(const Matrix &other) const {
		return szj == other.szi;
	}

	std::string Matrix::to_string() const {
		std::stringstream ss;

		ss << std::fixed << std::setprecision(4);

		ss << szi << " x " << szj << std::endl;
		for (int i = 0; i < szi; i++) {
			for (int j = 0; j < szj; j++) {
				ss << a[i][j];
				if (j != szj - 1) {
					ss << ", ";
				}
			}
			ss << ";\n";
		}

		return ss.str();
	}

	bool Matrix::equals(const Matrix &other, double eps) const {
		if (szi != other.szi || szj != other.szj) {
			return false;
		}

		for (int i = 0; i < szi; i++) {
			for (int j = 0; j < szj; j++) {
				if (std::abs(a[i][j] - other.a[i][j]) > eps) {
					return false;
				}
			}
		}

		return true;
	}

	Matrix Matrix::multiply(const Matrix &other) const {
		if (!compatible_with(other)) {
			throw std::invalid_argument("incompatible matrices");
		}

		Matrix C(szi, other.szj);
		for (int i = 0; i < C.szi; i++) {
			for (int j = 0; j < C.szj; j++) {
				double sum = 0;
				for (int k = 0; k < szj; k++) {
					sum += a[i][k] * other.a[k][j];
				}
				C.a[i][j] = sum;
			}
		}

		return C;
	}

	Matrix Matrix::add(const Matrix &other) const {
		if (szi != other.szi || szj != other.szj) {
			throw std::invalid_argument("incompatible matrices");	
		}

		Matrix C(szi, szj);
		for (int i = 0; i < C.szi; i++) {
			for (int j = 0; j < C.szj; j++) {
				C.a[i][j] = a[i][j] + other.a[i][j];
			}
		}

		return C;
	}

	Matrix Matrix::subtract(const Matrix &other) const {
		if (szi != other.szi || szj != other.szj) {
			throw std::invalid_argument("incompatible matrices");
		}

		Matrix C(szi, szj);
		for (int i = 0; i < C.szi; i++) {
			for (int j = 0; j < C.szj; j++) {
				C.a[i][j] = a[i][j] - other.a[i][j];
			}
		}

		return C;	
	}

	double Matrix::norm1() const {
		double maxsum = -1;
		for (int j = 0; j < szj; j++) {
			double sum = 0;
			for (int i = 0; i < szi; i++) {
				sum += std::abs(a[i][j]);
			}
			maxsum = std::max(maxsum, sum);
		}
		return maxsum;
	}

	bool Matrix::out_of_range(int i, int j) const {
		return i < 0 || i >= szi || j < 0 || j >= szj;
	}

	double Matrix::get(int i, int j) const {
		if (out_of_range(i, j)) {
			throw std::out_of_range("");
		}

		return a[i][j];
	}

	void Matrix::set(int i, int j, double val) {
		if (out_of_range(i, j)) {
			throw std::out_of_range("");
		}

		a[i][j] = val;
	}

	void Matrix::swap(int i, int j) {
		if (i < 0 || i >= szi || j < 0 || j >= szi) {
			throw std::out_of_range("");
		}

		for (int k = 0; k < szj; k++) {
			std::swap(a[i][k], a[j][k]);
		}
	}

	bool Matrix::lu(std::vector<std::pair<int, int>> &swaps) {
		for (int k = 0; k < std::min(szi, szj); k++) {
			double maxabs = -1;
			int best_i = -1;
			for (int i = k; i < szi; i++) {
				if (std::abs(a[i][k]) > maxabs) {
					maxabs = std::abs(a[i][k]);
					best_i = i;
				}
			}

			static const double EPS = 1e-12;
			if (maxabs < EPS) {
				return false;
			}
			
			this->swap(k, best_i);
			swaps.push_back(std::make_pair(k, best_i));

			for (int i = k + 1; i < szi; i++) {
				a[i][k] /= a[k][k];
				for (int j = k + 1; j < szj; j++) {
					a[i][j] -= a[i][k] * a[k][j] ;
				}
			}
		}

		return true;
	}

	void Matrix::swap(const std::vector<std::pair<int, int>> &swaps) {
		for (auto sw : swaps) {
			this->swap(sw.first, sw.second);
		}
	}

	Matrix Matrix::submatrix(int i_start, int j_start, int row_count, int col_count) const {
		if (out_of_range(i_start, j_start) || out_of_range(i_start + row_count - 1, j_start + col_count - 1)) {
			throw std::out_of_range("");
		}

		Matrix B(row_count, col_count);
		for (int i = 0; i < row_count; i++) {
			for (int j = 0; j < col_count; j++) {
				B.a[i][j] = a[i_start + i][j_start + j];
			}
		}
		return B;
	}

	void Matrix::submatrix(int i_start, int j_start, Matrix &B) const {
		if (out_of_range(i_start, j_start) || out_of_range(i_start + B.get_szi() - 1, j_start + B.get_szj() - 1)) {
			throw std::out_of_range("");
		}

		for (int i = 0; i < B.get_szi(); i++) {
			for (int j = 0; j < B.get_szj(); j++) {
				B.a[i][j] = a[i_start + i][j_start + j];
			}
		}
	}

	void Matrix::update_submatrix(int i_start, int j_start, const Matrix &B) {
		if (out_of_range(i_start, j_start) || out_of_range(i_start + B.get_szi() - 1, j_start + B.get_szj() - 1)) {
			throw std::out_of_range("");
		}

		for (int i = 0; i < B.get_szi(); i++) {
			for (int j = 0; j < B.get_szj(); j++) {
				a[i_start + i][j_start + j] = B.a[i][j];
			}
		}
	}

	Matrix::~Matrix() {
		if (a != nullptr) {
			Utils::free_contiguous<double>(a);	
		}
	}
}