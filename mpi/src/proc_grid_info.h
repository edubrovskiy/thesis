#ifndef PROC_GRID_INFO_H
#define PROC_GRID_INFO_H

#include <mpi.h>

namespace SignFunction {
	class ProcGridInfo {
	public:
		ProcGridInfo(int n, int world_size, int rank);

		int world_size,
			rank,
			n,						// matrix size
			proc_grid_size,
			proc_i, proc_j,			// coordinates of process in process grid
			block_size,			
			i_start, j_start,
			row_count, col_count,
			lu_j_start, lu_col_count;

		MPI_Comm row_comm, col_comm;
	};
}

#endif