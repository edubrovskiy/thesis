//#define DEBUG
#define SEQUENTIAL

#include <mpi.h>
#include <iostream>
#include "matrix.h"
#include "proc_grid_info.h"
#include "utils/log.h"
#include "utils/stopwatch.h"
#include "io.cpp"
#include <memory>
#include <cassert>

const int ROOT = 0;

namespace SignFunction {
	void bcast_matrix(Matrix &A, MPI_Comm comm) {
		MPI_Bcast(A.get_buf(), A.get_szi() * A.get_szj(), MPI_DOUBLE, ROOT, comm);
	}

	// void multiply(const char *left_path, const char *right_path, const char *res_path,
	// 	const ProcGridInfo &pginfo, Utils::Stopwatch &sw) {

	// 	sw.start("reading left matrix");
	// 	auto A = read_horizontal_strip(left_path, pginfo);
	// 	sw.log_time("reading left matrix");

	// 	sw.start("bcasting left matrix");
	// 	bcast_matrix(A, pginfo.row_comm);
	// 	sw.log_time("bcasting left matrix");

	// 	sw.start("reading right matrix");
	// 	auto B = read_vertical_strip(right_path, pginfo);
	// 	sw.log_time("reading right matrix");

	// 	sw.start("bcasting right matrix");
	// 	bcast_matrix(B, pginfo.col_comm);
	// 	sw.log_time("bcasting right matrix");

	// 	sw.start("multiplying");
	// 	auto C = A.multiply(B);
	// 	sw.log_time("multiplying");

	// 	sw.start("writing");
	// 	write_submatrix_at(C, res_path, pginfo);
	// 	sw.log_time("writing");
	// }

	void lu(const char *input_path, const char *res_path,
		const ProcGridInfo &pginfo, Utils::Stopwatch &sw, std::shared_ptr<Utils::Log> log) {

		int64_t displ = pginfo.j_start * sizeof(double);
		Matrix A(pginfo.n, pginfo.lu_col_count);
		read_submatrix(input_path, displ, pginfo.n, A);

		log->info("read " + A.to_string());

		write_submatrix_at(A, res_path, displ, pginfo.n);
	}
}

int main(int argc, char *argv[]) {
	using namespace SignFunction;

	MPI_Init(&argc, &argv);

	int rank, world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (argc < 4) {
		std::cout << "usage: matrix_mult <n> <matrix_path> <result_path>\n";
		MPI_Finalize();
		return 0;
	}

	int n = std::atoi(argv[1]);
	
	ProcGridInfo pginfo(n, world_size, rank);

	std::shared_ptr<Utils::Log> log(new Utils::Log(rank));
	Utils::Stopwatch sw(log);

	const char *matrix_path = argv[2];
	const char *res_path = argv[3];

	sw.start("parallel LU total");
	lu(matrix_path, res_path, pginfo, sw, log);
	sw.log_time("parallel LU total");

	// sw.start("parallel total");
	// multiply(left_path, right_path, res_path, pginfo, sw);
	// sw.log_time("parallel total");
		
#ifdef SEQUENTIAL
	if (rank == ROOT) {
		auto A = sequential_read_matrix(n, matrix_path);

		sw.start("sequential total");
		std::vector<std::pair<int, int>> swaps;
		//A.lu(swaps);
		sw.log_time("sequential total");
		
		// auto parallelA = sequential_read_matrix(n, res_path);
		// assert(A.equals(parallelA));
	}
#endif

	MPI_Comm_free(&pginfo.row_comm);
	MPI_Comm_free(&pginfo.col_comm);

	MPI_Finalize();
	return 0;
}