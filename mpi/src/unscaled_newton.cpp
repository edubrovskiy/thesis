//#define DEBUG
#define SEQUENTIAL

#include <mpi.h>
#include <iostream>
#include "matrix.h"
#include "proc_info.cpp"
#include "utils/log.h"
#include "utils/stopwatch.h"
#include "io.cpp"
#include <memory>
#include <cassert>
#include <vector>
#include <cmath>

const int ROOT = 0;

namespace SignFunction {
	void bcast(Matrix &A, int root, MPI_Comm comm) {
		MPI_Bcast(A.get_buf(), A.get_szi() * A.get_szj(), MPI_DOUBLE, root, comm);
	}

	void bcast(int &num, int root, MPI_Comm comm) {
		MPI_Bcast(&num, 1, MPI_INT, root, comm);	
	}

	void bcast(std::vector<int> &v, int n, int root, MPI_Comm comm) {
		MPI_Bcast(&v[0], n, MPI_INT, root, comm);
	}

	void bcast(std::vector<std::pair<int, int>> &v, int root, MPI_Comm comm) {
		int n = v.size();
		bcast(n, root, comm);

		v.resize(n);
		std::vector<int> v1(n), v2(n);
		for (int i = 0; i < n; i++) {
			v1[i] = v[i].first;
			v2[i] = v[i].second;
		}

		bcast(v1, n, root, comm);
		bcast(v2, n, root, comm);

		for (int i = 0; i < n; i++) {
			v[i].first = v1[i];
			v[i].second = v2[i];
		}
	}

	void fs(const Matrix &L, Matrix &A, int i_start) {
		int n = L.get_szj();
		for (int k = 0; k < A.get_szj(); k++) {
			for (int i = 0; i < n; i++) {
				double x = A.get(i_start + i, k);
				for (int j = 0; j < i; j++) {
					x -= L.get(i, j) * A.get(i_start + j, k);
				}
				A.set(i_start + i, k, x);
			}
		}
	}

	void fs(const Matrix &A, Matrix &B, const ProcInfo &pinfo) {
		int n = pinfo.n;
		for (int proc = 0; proc < pinfo.worker_count; proc++) {
			int l_start = pinfo.j_start;
			int l_row_count = n - l_start;
			int l_col_count = pinfo.col_count;

			MPI_Comm comm = pinfo.worker_comm;

			bcast(l_start, proc, comm);
			bcast(l_row_count, proc, comm);
			bcast(l_col_count, proc, comm);

			Matrix L(l_row_count, l_col_count);
			if (pinfo.rank == proc) {
				A.submatrix(l_start, 0, L);
			}

			bcast(L, proc, comm);

			fs(L, B, l_start);
			for (int i = l_start + L.get_szj(); i < n; i++) {
				for (int j = 0; j < B.get_szj(); j++) {
					double sum = 0;
					for (int k = 0; k < L.get_szj(); k++) {
						sum += L.get(i - l_start, k) * B.get(l_start + k, j);
					}
					B.set(i, j, B.get(i, j) - sum);
				}
			}
		}
	}

	void bs(const Matrix &U, Matrix &Y, int i_end) {
		int n = U.get_szj();
		int szi = U.get_szi();
		for (int k = 0; k < Y.get_szj(); k++) {
			for (int i = 0; i < n; i++) {
				double y = Y.get(i_end - i, k);
				for (int j = 0; j < i; j++) {
					y -= U.get(szi - 1 - i, n - 1 - j) * Y.get(i_end - j, k);
				}
				y /= U.get(szi - 1 - i, n - 1 - i);
				Y.set(i_end - i, k, y);
			}
		}
	}

	void bs(const Matrix &A, Matrix &Y, const ProcInfo &pinfo) {
		for (int proc = pinfo.worker_count - 1; proc >= 0; proc--) {
			int u_col_count = pinfo.col_count;
			int u_end = pinfo.j_start + u_col_count - 1;
			int u_row_count = u_end + 1;

			MPI_Comm comm = pinfo.worker_comm;

			bcast(u_end, proc, comm);
			bcast(u_col_count, proc, comm);
			bcast(u_row_count, proc, comm);

			Matrix U(u_row_count, u_col_count);
			if (pinfo.rank == proc) {
				A.submatrix(0, 0, U);
			}

			bcast(U, proc, comm);

			bs(U, Y, u_end);

			int sz = U.get_szj();
			for (int i = u_end - sz; i >= 0; i--) {
				for (int j = 0; j < Y.get_szj(); j++) {
					double sum = 0;
					for (int k = 0; k < sz; k++) {
						sum += U.get(i, sz - 1 - k) * Y.get(u_end - k, j);
					}
					Y.set(i, j, Y.get(i, j) - sum);
				}
			}
		}
	}

	void lu(Matrix &A, const ProcInfo &pinfo, Utils::Stopwatch &sw,
		std::vector<std::pair<int, int>> &all_swaps) {
		
		for (int k = 0; k < pinfo.worker_count; k++) {
			int l_start = pinfo.j_start;
			int l_row_count = pinfo.n - l_start;
			int l_col_count = A.get_szj();

			bcast(l_start, k, pinfo.worker_comm);
			bcast(l_row_count, k, pinfo.worker_comm);
			bcast(l_col_count, k, pinfo.worker_comm);

			Matrix L(l_row_count, l_col_count);

			std::vector<std::pair<int, int>> swaps;
			if (pinfo.rank == k) {
				A.submatrix(l_start, 0, L);

				L.lu(swaps);

				A.update_submatrix(l_start, 0, L);

				for (size_t i = 0; i < swaps.size(); i++) {
					swaps[i].first += l_start;
					swaps[i].second += l_start;
				}
			}

			bcast(swaps, k, pinfo.worker_comm);
			all_swaps.insert(all_swaps.end(), swaps.begin(), swaps.end());
			if (pinfo.rank != k) {
				A.swap(swaps);
			}

			bcast(L, k, pinfo.worker_comm);

			if (pinfo.rank <= k) {
				continue;
			}

			fs(L, A, l_start);

			for (int i = l_start + L.get_szj(); i < A.get_szi(); i++) {
				for (int j = 0; j < A.get_szj(); j++) {
					double sum = 0;
					for (int r = 0; r < L.get_szj(); r++) {
						sum += L.get(i - l_start, r) * A.get(l_start + r, j);
					}
					A.set(i, j, A.get(i, j) - sum);
				}
			}
		}
	}

	double det_scaling(const Matrix &U, const ProcInfo &pinfo) {
		double sum = 0;
		for (int i = 0; i < U.get_szj(); i++) {
			sum += std::log(std::abs(U.get(pinfo.j_start + i, i)));
		}
		
		std::cout << "proc " << pinfo.rank << " has " << sum << std::endl;

		double total_sum;
		MPI_Allreduce(&sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, pinfo.worker_comm);

		if (pinfo.rank == 0) {
			std::cout << "total sum = " << sum << std::endl;			
		}

		return std::exp(-total_sum / pinfo.n);
	}

	Matrix next(const Matrix &X, std::shared_ptr<Matrix> &nextX, const ProcInfo &pinfo, Utils::Stopwatch &sw) {
		Matrix B = X.clone();
		std::vector<std::pair<int, int>> swaps;

		lu(B, pinfo, sw, swaps);

		nextX = std::shared_ptr<Matrix>(new Matrix(pinfo.n, pinfo.col_count));
		for (int i = 0; i < nextX->get_szi(); i++) {
			for (int j = 0; j < nextX->get_szj(); j++) {
				nextX->set(i, j, 0);
			}
		}
		for (int i = 0; i < nextX->get_szj(); i++) {
			nextX->set(pinfo.j_start + i, i, 1);
		}
		nextX->swap(swaps);

		fs(B, *nextX, pinfo);
		bs(B, *nextX, pinfo);

		for (int i = 0; i < B.get_szi(); i++) {
			for (int j = 0; j < B.get_szj(); j++) {
				nextX->set(i, j, (X.get(i, j) + nextX->get(i, j)) / 2);
			}
		}
		return B;
	}

	double norm(const Matrix &A, MPI_Comm comm) {
		double n = A.norm1();
		double max_n;
		MPI_Allreduce(&n, &max_n, 1, MPI_DOUBLE, MPI_MAX, comm);
		return max_n;
	}

	void calc(const char *input_path, const char *res_path,
		const ProcInfo &pinfo, Utils::Stopwatch &sw, std::shared_ptr<Utils::Log> log) {

		int64_t displ = pinfo.j_start * sizeof(double);
		std::shared_ptr<Matrix> X(new Matrix(pinfo.n, pinfo.col_count));
		read_submatrix(input_path, displ, pinfo.n, *X);

		static const double INF = 1e300;
		static const double TOL = 1e-9;

		sw.start("parallel total");

		double err = INF;
		while (err > TOL) {
			std::shared_ptr<Matrix> nextX;
			next(*X, nextX, pinfo, sw);
			err = norm(nextX->subtract(*X), pinfo.worker_comm) / norm(*nextX, pinfo.worker_comm);
			X = nextX;
		}

		sw.log_time("parallel total");

		write_submatrix_at(*X, res_path, displ, pinfo.n);
	}

	void calc_sequential(const char *input_path, const char *res_path,
		const ProcInfo &pinfo, Utils::Stopwatch &sw, std::shared_ptr<Utils::Log> log) {

		int n = pinfo.n;
		std::shared_ptr<Matrix> X = sequential_read_matrix(n, input_path);

		static const double INF = 1e300;
		static const double TOL = 1e-9;

		sw.start("sequential total");

		double err = INF;
		while (err > TOL) {
			Matrix B = X->clone();
			std::vector<std::pair<int, int>> swaps;
			B.lu(swaps);

			std::shared_ptr<Matrix> nextX(Matrix::identity(n));
			nextX->swap(swaps);

			fs(B, *nextX, 0);
			bs(B, *nextX, n - 1);

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					nextX->set(i, j, (X->get(i, j) + nextX->get(i, j)) / 2);
				}
			}

			err = nextX->subtract(*X).norm1() / nextX->norm1();
			X = nextX;
		}

		sw.log_time("sequential total");
	}
}

int main(int argc, char *argv[]) {
	using namespace SignFunction;

	MPI_Init(&argc, &argv);

	int rank, world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (argc < 4) {
		std::cout << "usage: " << argv[0] << " <n> <matrix_path> <result_path>\n";
		MPI_Finalize();
		return 0;
	}

	int n = std::atoi(argv[1]);
	
	ProcInfo pinfo(n, world_size, rank);
	if (rank >= pinfo.worker_count) {
		MPI_Finalize();
		return 0;
	}

	std::shared_ptr<Utils::Log> log(new Utils::Log(rank));
	Utils::Stopwatch sw(log);

	const char *matrix_path = argv[2];
	const char *res_path = argv[3];

	calc(matrix_path, res_path, pinfo, sw, log);
#ifdef SEQUENTIAL
	if (rank == ROOT) {
		auto Spar = sequential_read_matrix(n, res_path);

		calc_sequential(matrix_path, res_path, pinfo, sw, log);

		auto Sseq = sequential_read_matrix(n, res_path);

		if (!Spar->equals(*Sseq, 1e-7)) {
			std::cout << "error\n";
			std::cout << "Spar: " << Spar->to_string();
			std::cout << "Sseq: " << Sseq->to_string();
		}

		// auto A = sequential_read_matrix(n, matrix_path);
		// auto S = sequential_read_matrix(n, res_path);

		// if (!S.multiply(S).equals(Matrix::identity(S.get_szi()), 1e-7)) {

		// }
	}
#endif

	MPI_Comm_free(&pinfo.worker_comm);

	MPI_Finalize();
	return 0;
}