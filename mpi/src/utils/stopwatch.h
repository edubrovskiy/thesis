#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <map>
#include <iostream>
#include <memory>
#include "log.h"

namespace Utils {
	class Stopwatch {
	public:
		Stopwatch(std::shared_ptr<Log> log);
		void ignore();
		void start(const std::string &action);
		void log_time(const std::string &action) const;
	private:
		bool _ignore = false;
		std::map<std::string, double> start_time;
		std::shared_ptr<Log> log;
	};
}

#endif
