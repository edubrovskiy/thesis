#ifndef LOG_H
#define LOG_H

#include <fstream>
#include <sstream>

namespace Utils {
	class Log {
	public:
		Log(const std::string &path) {
			open_log(path);
		}

		Log(int rank) {
			static const std::string DEFAULT_LOG_DIR = "logs";

			std::stringstream ss;
			ss << DEFAULT_LOG_DIR << '/' << rank << ".txt";
			open_log(ss.str());
		}
		
		template<typename T>
		void info(const T &item) {
			print_with_tag(item, "INFO");
		}

		template<typename T>
		void warn(const T &item) {
			print_with_tag(item, "WARN");
		}
	private:
		std::ofstream log;
		
		void open_log(const std::string &path) {
			log.open(path, std::ios_base::trunc | std::ios_base::out);
			if (!log.is_open()) {
				// TODO: add special exception
				throw "failed to open log";
			}
		}

		template<typename T>
		void print_with_tag(const T &item, const std::string &tag) {
			log << "[" << tag << "]:\t" << item << std::endl;		
		}
	};
}

#endif
