#include <mpi.h>
#include "stopwatch.h"
#include <stdexcept>
#include <sstream>
#include <iomanip>

namespace Utils {
	Stopwatch::Stopwatch(std::shared_ptr<Log> log) {
		if (log == nullptr) {
			throw std::invalid_argument("log");
		}
		this->log = log;
	}

	void Stopwatch::ignore() {
		_ignore = true;
	}

	void Stopwatch::start(const std::string &action) {
		if (_ignore) return;

		start_time[action] = MPI_Wtime();
	}

	void Stopwatch::log_time(const std::string &action) const {
		if (_ignore) return;

		auto it = start_time.find(action);
		if (it == start_time.end()) {
			log->warn("unknown action " + action);
			return;
		}

		double stop_time = MPI_Wtime();

		std::stringstream ss;
		ss << std::fixed << std::setprecision(3);
		ss << it->first << " took " << stop_time - it->second << " seconds";
		log->info(ss.str());
	}
}
