#ifndef ALLOC_CONTIGUOUS_H
#define ALLOC_CONTIGUOUS_H

namespace Utils {
	template<typename T>
	T** alloc_contiguous(int row_count, int col_count) {
		T *storage = new T[row_count * col_count];
		T **row_ptrs = new T*[row_count];

		for (int i = 0; i < row_count; i++, storage += col_count) {
			row_ptrs[i] = storage;
		}
		
		return row_ptrs;
	}

	template<typename T>
	void free_contiguous(T** array) {
		delete[] array[0];
		delete[] array;
	}
}

#endif